#include "WifiCtl.h"

WifiCtl::WifiCtl(const char *ssid, const char *passphrase, const char *hostname,
                 CWifi &wifi, WiFiClient &client)
    : _ssid(ssid),
      _passphrase(passphrase),
      _hostname(hostname),
      _wifi(wifi),
      _client(client)
{
  // take care some stuff might not be created yet that you want to use here
  // like Serial
  // when the object ob this class gets created in main.cpp
  // the setup did not run yet, hence there is no Serial connection yet
}

CWifi WifiCtl::getWifi() { return _wifi; }

WiFiClient WifiCtl::getClient() { return _client; }

void WifiCtl::connect(void (*errorHandler)())
{
  _wifi.disconnect();
  Serial.println(
      "WifiCtl.connect - disconnected all Wifis, waiting for things to cool "
      "down...");
  delay(10 * 1000);
  _wifi.setHostname(_hostname);
  Serial.print("WifiCtl.connect - connecting to Wifi");
  _wifi.begin(_ssid, _passphrase);

  int wifiConnectLoops = 0;

  while (_wifi.status() != WL_CONNECTED)
  {
    wifiConnectLoops++;
    Serial.print(".");
    delay(2000);
    if (wifiConnectLoops > 10)
    {
      Serial.println();
      Serial.println("WifiCtl.connect - connection to Wifi failed!");
      errorHandler();
    }
  }
  Serial.println();
  Serial.print("WifiCtl.connect - connected with IP Address: ");
  Serial.println(WiFi.localIP());
}

bool WifiCtl::status()
{
  if (_wifi.status() == 3)
  {
    return 1;
  }
  return 0;
}

void WifiCtl::disconnect() { _wifi.disconnect(); }