#ifndef WIFICTL_h
#define WIFICTL_h

#include <Arduino.h>
#include <WiFiS3.h>

class WifiCtl
{
public:
        WifiCtl(const char *ssid, const char *passphrase, const char *hostname,
                CWifi &wifi, WiFiClient &client);
        CWifi getWifi();
        WiFiClient getClient();
        void connect(void (*errorHandler)());
        bool status();
        //   void reconnect() { connect(); };
        void disconnect();

private:
        const char *_ssid;
        const char *_passphrase;
        const char *_hostname;
        CWifi &_wifi;
        WiFiClient &_client;
};

#endif